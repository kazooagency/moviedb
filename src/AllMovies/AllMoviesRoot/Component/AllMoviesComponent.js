import React, { useState, useEffect } from "react";
import axios from "axios";
import styled from "styled-components";

import CardComponent from "../../../Card/Component/CardComponent";
import PaginationContainerComponent from "../../../PaginationContainer/Component/PaginationContainerComponent";
import DropdownComponent from "../../../Dropdown/Component/DropdownComponent";

import {
  API_BASE_URL,
  API_KEY,
  DISCOVER_MOVIES_ENDPOINT,
  GENRE_MOVIE_LIST_ENDPOINT
} from "../../../Config/Api";

import { MobileOnly } from "../../../Config/Breakpoints";
import { Title, lightblue } from "../../../Config/Styles";

const GlobalContainer = styled.div``;

const MovieContainer = styled.div`
  display: flex;
  flex-wrap: wrap;
  justify-content: space-between;
  margin: 0 -20px;
  @media (${MobileOnly}) {
    justify-content: space-around;
    margin: 0 -20px;
  }
`;

const FilterContainer = styled.div`
  display: flex;
  align-items: center;
  margin-bottom: 20px;
  @media (${MobileOnly}) {
  }
`;

const Subtitle = styled.span`
  color: ${lightblue};
  font-family: Roboto;
  font-size: 12px;
  font-weight: 400;
  margin-right: 10px;
`;

const fetchGenres = () => {
  return new Promise((resolve, reject) => {
    axios
      .get(
        `${API_BASE_URL}${GENRE_MOVIE_LIST_ENDPOINT}?api_key=${API_KEY}&language=en-US`
      )
      .then(res => {
        resolve(res.data);
      })
      .catch(err => reject(err));
  });
};

function AllMoviesComponent() {
  const [allMovies, setAllMovies] = useState([]);
  const [genreIsOpen, setGenreIsOpen] = useState(false);
  const [selectedGenre, setSelectedGenre] = useState(null);
  const [genres, setGenres] = useState([{ id: null, name: "Tous" }]);
  const [page, setPage] = useState(1);
  const [totalPages, setTotalPages] = useState(null);

  useEffect(() => {
    fetchMovies()
      .then(res => {
        setAllMovies(res.results);
        setTotalPages(res.total_pages);
      })
      .catch(err => {
        console.log(err);
      });
  }, [selectedGenre, page]);

  useEffect(() => {
    fetchGenres()
      .then(res => {
        setGenres([...genres, ...res.genres]);
      })
      .catch(err => {
        console.log(err);
      });
  }, []);

  const fetchMovies = () => {
    let url = `${API_BASE_URL}${DISCOVER_MOVIES_ENDPOINT}?api_key=${API_KEY}&language=en-US`;
    if (selectedGenre && selectedGenre.id) {
      url += `&with_genres=${selectedGenre.id}`;
    }
    url += `&page=${page}`;
    return new Promise((resolve, reject) => {
      axios
        .get(url)
        .then(res => {
          resolve(res.data);
        })
        .catch(err => reject(err));
    });
  };

  const handleClickGenre = () => {
    if (genreIsOpen) {
      setGenreIsOpen(false);
    } else {
      setGenreIsOpen(true);
    }
  };

  const handeClickOption = ({ id, name }) => {
    setPage(1);
    setSelectedGenre({ id, name });
    fetchMovies();
    setGenreIsOpen(false);
  };

  return (
    <GlobalContainer>
      <Title>Tous les films</Title>
      <FilterContainer>
        <Subtitle>Filtrer par:</Subtitle>
        <DropdownComponent
          handleClickDropDown={handleClickGenre}
          handeClickOption={handeClickOption}
          dropdownName={"Genre"}
          options={genres}
          optionValue={selectedGenre}
          isOpen={genreIsOpen}
        />
      </FilterContainer>

      <MovieContainer>
        {allMovies.map(movie => (
          <CardComponent
            title={movie.title}
            date={movie.release_date}
            imgsrc={movie.poster_path}
            key={movie.id}
          />
        ))}
      </MovieContainer>
      <PaginationContainerComponent
        page={page}
        totalPages={totalPages}
        setPage={setPage}
      />
    </GlobalContainer>
  );
}

export default AllMoviesComponent;
