import React from "react";
import styled from "styled-components";

import { MobileOnly } from "../../Config/Breakpoints";

import { white, lightblue } from "../../Config/Styles";

import { IMG_URL } from "../../Config/Api";

const Title = styled.p`
  font-size: 14px;
  font-weight: 700;
  font-family: Roboto;
  color: ${white};
  margin: 10px 0 5px;
`;

const Date = styled.p`
  color: ${lightblue};
  font-family: Roboto;
  font-size: 12px;
  font-weight: 400;
  margin: 0;
`;

const getImgSrc = imgsrc => `${IMG_URL}${imgsrc}`;
const getYearFromDate = date => date.split("-")[0];

function CardComponent({ imgsrc, title, date, parent }) {
  const Container = styled.div`
    margin: 0 20px;
    width: 138px;
    min-height: 270px;
    @media (${MobileOnly}) {
      margin: 0 19px;
      ${parent === "TOPMOVIES" &&
        `
        width: 86px;
        min-height: 210px;
        height: 210px
      `}
    }
  `;

  const Image = styled.img`
    border-radius: 5%;
    height: 200px;
    width: 138px;
    object-fit: cover;
    @media (${MobileOnly}) {
      ${parent === "TOPMOVIES" &&
        `
        width: 86px;
        height: 124px
      `}
    }
  `;

  const ImageContainer = styled.div`
    height: 200px;
    width: 138px;
    @media (${MobileOnly}) {
      ${parent === "TOPMOVIES" &&
        `
        width: 86px;
        height: 124px
    `}
    }
  `;

  return (
    <Container>
      <ImageContainer>
        <Image src={getImgSrc(imgsrc)} alt={title} />
      </ImageContainer>
      <Title>{title}</Title>
      <Date>{getYearFromDate(date)}</Date>
    </Container>
  );
}

export default CardComponent;
