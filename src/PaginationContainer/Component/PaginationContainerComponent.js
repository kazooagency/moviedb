import React from "react";
import styled from "styled-components";

import PaginationButton from "../../PaginationButton/Component/PaginationButtonComponent";
import PaginationPreviousIcon from "../../assets/img/paginationpreviousarrow.svg";
import PaginationNextIcon from "../../assets/img/paginationnextarrow.svg";

const GlobalContainer = styled.div`
  margin: 50px 0;
  display: flex;
  justify-content: center;
  flex-wrap: wrap;
`;

function PaginationContainerComponent({ page, setPage, totalPages }) {
  const pagination = () => {
    const pages = [page];
    let i;
    for (i = 1; page - i > 0 && i < 5; i++) {
      pages.unshift(page - i);
    }
    i = 1;
    while (pages.length < 10 && page + i <= totalPages) {
      pages.push(page + i);
      i++;
    }

    return pages;
  };

  const handleClickPrevious = () => {
    if (page > 1) {
      setPage(page - 1);
    }
  };
  const handleClickNext = () => {
    if (page < totalPages) {
      setPage(page + 1);
    }
  };
  return (
    <GlobalContainer>
      <PaginationButton
        key={"previous"}
        page={page}
        handleClick={handleClickPrevious}
      >
        <img alt={"previous"} src={PaginationPreviousIcon} />
      </PaginationButton>
      {pagination().map(el => (
        <PaginationButton
          key={el}
          page={page}
          handleClick={() => {
            setPage(el);
          }}
        >
          {el}
        </PaginationButton>
      ))}
      <PaginationButton key={"next"} page={page} handleClick={handleClickNext}>
        <img alt={"next"} src={PaginationNextIcon} />
      </PaginationButton>
    </GlobalContainer>
  );
}

export default PaginationContainerComponent;
