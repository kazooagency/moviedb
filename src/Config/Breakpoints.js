const desktop_resolution = 1050;

export const MobileOnly = `max-width: ${desktop_resolution}px`;
export const DesktopOnly = `max-width: ${desktop_resolution}px`;
