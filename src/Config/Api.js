export const API_BASE_URL = "https://api.themoviedb.org/3/";
export const API_KEY = "b64909a0a7280e6e6c295aea6cc3b04f";
export const DISCOVER_MOVIES_ENDPOINT = "discover/movie";
export const GENRE_MOVIE_LIST_ENDPOINT = "genre/movie/list";
export const IMG_URL = "https://image.tmdb.org/t/p/w185/";
