import styled from "styled-components";

export const lightblue = "#586e94";
export const darkblue = "#253755";
export const darkerblue = "#0d1d38";
export const white = "#ffffff";
export const lightgrey = "#1e2d48";

export const Title = styled.h2`
  color: ${white};
  font-family: Roboto;
  font-size: 26px;
  font-weight: 700;
`;
