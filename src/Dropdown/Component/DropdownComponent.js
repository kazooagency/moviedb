import React from "react";
import styled from "styled-components";

import { lightblue, darkblue, darkerblue } from "../../Config/Styles";

import DropDownArrow from "../../assets/img/arrow_down.png";

const GlobalContainer = styled.div`
  height: 44px;
`;

const OptionContainer = styled.div`
  display: ${props => (props.isOpen ? "block" : "none")};
  position: absolute;
`;
const Dropdown = styled.div`
  display: flex;
  justify-content: space-between;
  align-items: center;
  width: 150px;
  border: 1px solid ${lightblue};
  ${props => (props.isOpen ? "border-bottom: none" : "")};
  background-color: ${props => (props.isOpen ? darkblue : darkerblue)};
  padding: 14px;
  border-radius: ${props => (props.isOpen ? "14px 14px 0 0" : "14px")};
  color: ${lightblue};
  box-shadow: 0 10px 20px rgba(0, 0, 0, 0.1);
  font-family: Roboto;
  font-size: 12px;
  font-weight: 700;
  cursor: pointer;
`;

const Option = styled.div`
  width: 150px;
  border: 1px solid ${lightblue};
  background-color: ${props => (props.isOpen ? darkblue : darkerblue)};
  padding: 14px 50px 14px 14px;
  color: ${lightblue};
  box-shadow: 0 10px 20px rgba(0, 0, 0, 0.1);
  font-family: Roboto;
  font-size: 12px;
  font-weight: 700;
  border-bottom: none;
  &:last-child {
    border-radius: 0 0 14px 14px;
    border-bottom: 1px solid ${lightblue};
  }
  cursor: pointer;
`;

function DropdownComponent({
  handleClickDropDown,
  handeClickOption,
  dropdownName,
  options,
  optionValue,
  isOpen
}) {
  return (
    <GlobalContainer>
      <Dropdown isOpen={isOpen} onClick={handleClickDropDown}>
        <span>
          {optionValue && optionValue.name ? optionValue.name : dropdownName}
        </span>
        <img alt={"arrow"} src={DropDownArrow} />
      </Dropdown>
      <OptionContainer isOpen={isOpen}>
        {options.map(option => {
          return (
            <Option
              isOpen={isOpen}
              key={option.id}
              onClick={() => handeClickOption(option)}
            >
              {option.name}
            </Option>
          );
        })}
      </OptionContainer>
    </GlobalContainer>
  );
}

export default DropdownComponent;
