import React from "react";
import logosrc from "../../assets/img/logo.svg";
import searchicon from "../../assets/img/searchicon.svg";

import styled from "styled-components";

import { MobileOnly } from "../../Config/Breakpoints";
import { darkblue, lightblue } from "../../Config/Styles";

const Input = styled.input`
  background-color: ${darkblue};
  padding: 14px 50px 14px 14px;
  border-radius: 14px;
  color: ${lightblue};
  border: none;
  box-shadow: 0 10px 20px rgba(0, 0, 0, 0.1);
  width: 315px;
  font-family: Roboto;
  font-size: 12px;
  font-weight: 400;
  &::placeholder {
    color: ${lightblue};
  }
  @media (${MobileOnly}) {
    width: 100%;
    font-size: 16px;
  }
`;

const GlobalContainer = styled.div`
  display: flex;
  flex-direction: row;
  justify-content: space-between;
  margin-bottom: 60px;
  @media (${MobileOnly}) {
    flex-direction: column;
    margin-bottom: 30px;
  }
`;
const InputContainer = styled.div`
  display: flex;
  flex-direction: row;
`;

const Logo = styled.img`
  @media (${MobileOnly}) {
    align-self: flex-start;
    margin-bottom: 20px;
  }
`;

const SearchIcon = styled.img``;

const SearchIconContainer = styled.div`
  display: flex;
  align-items: center;
  justify-content: center;
  width: 40px;
  height: 40px;
  margin-left: -40px;
`;

function Header() {
  return (
    <GlobalContainer>
      <Logo src={logosrc} alt="logo" />
      <InputContainer>
        <Input placeholder="Rechercher un film" />
        <SearchIconContainer>
          <SearchIcon src={searchicon} alt="search" />
        </SearchIconContainer>
      </InputContainer>
    </GlobalContainer>
  );
}

export default Header;
