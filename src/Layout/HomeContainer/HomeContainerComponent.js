import React from "react";
import styled from "styled-components";

import { MobileOnly } from "../../Config/Breakpoints";
import { lightgrey } from "../../Config/Styles";

const Separator = styled.div`
  height: 1px;
  background-color: ${lightgrey};
  margin: 45px 0 70px 0;
  @media (${MobileOnly}) {
    margin: 0 20px;
  }
`;

function HomeContainerComponent({
  header,
  topMoviesContainer,
  allMoviesContainer
}) {
  return (
    <>
      {header}
      {topMoviesContainer}
      <Separator />
      {allMoviesContainer}
    </>
  );
}

export default HomeContainerComponent;
