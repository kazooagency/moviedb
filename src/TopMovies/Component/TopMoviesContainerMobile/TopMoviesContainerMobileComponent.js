import React from "react";
import styled from "styled-components";
import CardComponent from "../../../Card/Component/CardComponent";

import { MobileOnly } from "../../../Config/Breakpoints";

const GlobalContainer = styled.div`
  display: none;
  flex-direction: column;

  @media (${MobileOnly}) {
    display: flex;
  }
`;

const MoviesContainer = styled.div`
  margin: 0 -30px 0 -19px;
  display: flex;
  flex-direction: row;
  overflow: scroll;
  &&::-webkit-scrollbar {
    width: 0 !important;
  }
  scrollbar-width: none;
  first-child {
    background-color: green;
  }
`;

const Title = styled.h2`
  font-size: 16px;
`;

function TopMoviesContainerMobileComponent({ topMovies, getYear, title }) {
  return (
    <GlobalContainer>
      <Title>{title}</Title>
      <MoviesContainer>
        {topMovies.map(movie => (
          <CardComponent
            title={movie.title}
            date={movie.release_date}
            imgsrc={movie.poster_path}
            key={movie.id}
            parent={"TOPMOVIES"}
          />
        ))}
      </MoviesContainer>
    </GlobalContainer>
  );
}

export default TopMoviesContainerMobileComponent;
