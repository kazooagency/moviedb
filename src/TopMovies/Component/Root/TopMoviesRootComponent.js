import React, { useState, useEffect } from "react";
import axios from "axios";

import TopMoviesContainerDesktop from "../TopMoviesContainerDesktop/TopMoviesContainerDesktopComponent";
import TopMoviesContainerMobile from "../TopMoviesContainerMobile/TopMoviesContainerMobileComponent";

function TopMoviesRootComponent() {
  const [topMovies, setTopMovies] = useState([]);

  useEffect(() => {
    async function fetchTopMovies() {
      const result = await axios(
        "https://api.themoviedb.org/3/movie/top_rated?api_key=b64909a0a7280e6e6c295aea6cc3b04f&language=en-US&page=1"
      );
      setTopMovies(result.data.results.slice(0, 10));
    }
    fetchTopMovies();
  }, []);

  const title = "Les 10 meilleurs films";

  return (
    <>
      <TopMoviesContainerDesktop topMovies={topMovies} title={title} />
      <TopMoviesContainerMobile topMovies={topMovies} title={title} />
    </>
  );
}

export default TopMoviesRootComponent;
