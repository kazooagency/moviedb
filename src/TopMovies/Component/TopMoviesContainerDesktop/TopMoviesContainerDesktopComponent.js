import React, { useState } from "react";
import styled from "styled-components";
import CardComponent from "../../../Card/Component/CardComponent";

import previous from "../../../assets/img/previous.png";
import next from "../../../assets/img/next.png";

import { MobileOnly } from "../../../Config/Breakpoints";
import { Title } from "../../../Config/Styles";

function TopMoviesContainerDesktopComponent({ topMovies, getYear, title }) {
  const [offset, setOffset] = useState(0);

  const handleClickPrevious = () => {
    setOffset((offset - 1) % topMovies.length);
  };

  const handleClickNext = () => {
    setOffset((offset + 1) % topMovies.length);
  };

  const getIndex = distance =>
    (offset + distance + topMovies.length) % topMovies.length;

  const displayedTopMovies = [];

  if (topMovies.length > 0) {
    for (let i = 0; i <= 3; i++) {
      displayedTopMovies.push(
        <CardComponent
          title={topMovies[getIndex(i)].title}
          date={topMovies[getIndex(i)].release_date}
          imgsrc={topMovies[getIndex(i)].poster_path}
          key={topMovies[getIndex(i)].id}
          parent={"TOPMOVIES"}
        />
      );
    }
  }

  const GlobalContainer = styled.div`
    display: flex;
    flex-direction: column;
    overflow: hidden;
    @media (${MobileOnly}) {
      display: none;
    }
  `;

  const MovieContainer = styled.div`
    display: flex;
    flex-direction: row;
    align-items: center;
    justify-content: space-between;
    margin-top: 20px;
  `;

  const Previous = styled.img`
    margin-right: -30px;
  `;

  const Next = styled.img`
    margin-left: -30px;
  `;

  return (
    <GlobalContainer>
      <Title>{title}</Title>
      <MovieContainer>
        <Previous alt="previous" src={previous} onClick={handleClickPrevious} />
        {displayedTopMovies.map(el => el)}
        <Next alt="next" src={next} onClick={handleClickNext} />
      </MovieContainer>
    </GlobalContainer>
  );
}

export default TopMoviesContainerDesktopComponent;
