import React from "react";
import styled from "styled-components";
import "./App.css";

import { MobileOnly } from "./Config/Breakpoints";

import bgimg from "./assets/img/bg-img.png";

import HomeContainerComponent from "./Layout/HomeContainer/HomeContainerComponent";
import Header from "./Header/Component/HeaderComponent";
import TopMoviesRootComponent from "./TopMovies/Component/Root/TopMoviesRootComponent";
import AllMoviesComponent from "./AllMovies/AllMoviesRoot/Component/AllMoviesComponent";

const AppBackground = styled.div`
  background: #0d1d38 url(${bgimg});
  background-size: cover;
  height: 100%;
  background-blend-mode: luminosity;
`;

const AppContainer = styled.div`
  width: 1025px;
  margin: 0 auto;
  padding: 30px;
  @media (${MobileOnly}) {
    width: 100%;
  }
`;

function App() {
  return (
    <AppBackground>
      <AppContainer>
        <HomeContainerComponent
          header={<Header />}
          topMoviesContainer={<TopMoviesRootComponent />}
          allMoviesContainer={<AllMoviesComponent />}
        />
      </AppContainer>
    </AppBackground>
  );
}

export default App;
