import React from "react";
import styled from "styled-components";

import { lightblue, darkerblue, darkblue } from "../../Config/Styles";

const PaginationButton = styled.span`
  background-color: ${props =>
    props.children === props.page ? lightblue : darkerblue};
  width: 40px;
  height: 40px;
  box-shadow: 0 10px 20px rgba(0, 0, 0, 0.1);
  border-radius: 14px;
  border: 2px solid ${lightblue};
  color: ${darkblue};
  font-family: Roboto;
  font-size: 16px;
  font-weight: 900;
  margin: 5px;
  display: inline-block;
  padding: 0px;
  text-align: center;
  line-height: 40px;
  cursor: pointer;
`;

function PaginationButtonComponent({ children, page, handleClick }) {
  return (
    <PaginationButton onClick={handleClick} page={page}>
      {children}
    </PaginationButton>
  );
}

export default PaginationButtonComponent;
